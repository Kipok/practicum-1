#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int n, k;
int a[15];

void printans(int x) {
    if (x == k + 1) {
        for (int i = 1; i <= k; i++) {
            printf("%d ", a[i]);
        }
        printf("\n");
        return;
    }
    for (int i = a[x - 1] + 1; i < n; i++) {
        a[x] = i;
        printans(x + 1);
    }
}

int
main(void)
{
    scanf("%d%d", &n, &k);
    a[0] = -1;
    printans(1);
    return 0;
}
