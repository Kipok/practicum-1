#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int isprime(int x) {
    if (x == 1) {
        return 0;
    }
    for (int i = 2; i * i <= x; i++) {
        if (x % i == 0) {
            return 0;
        }
    }
    return 1;
}

int
main(void)
{
    int x;
    scanf("%d", &x);
    while (!isprime(x)) {
        ++x;
    }
    printf("%d", x);
    return 0;
}
