#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define max(a, b) (a) > (b) ? (a) : (b)

enum {
    MAXSIZE = 1000001
};

typedef struct sequence {
    int l, r;
} sequence;

int less(sequence a, sequence b) {
    return a.l < b.l || (a.l == b.l && a.r < b.r);
}

int more(sequence a, sequence b) {
    return a.l > b.l || (a.l == b.l && a.r > b.r);
}

void swap(sequence *a, sequence *b) {
    sequence c = *a;
    *a = *b;
    *b = c;
}

sequence a[MAXSIZE], c[MAXSIZE];

void mergeSort(int l, int r) {
    if (l == r)
        return;
    int m = (l + r) / 2;
    mergeSort(l, m);
    mergeSort(m + 1, r);
    int i = l, j = m + 1, t = l;
    while (i <= m && j <= r) {
        if (less(a[i], a[j])) {
            c[t++] = a[i++];
        } else {
            c[t++] = a[j++];
        }
    }
    for (int q = i; q <= m; q++) {
        c[t++] = a[q];
    }
    for (int q = j; q <= r; q++) {
        c[t++] = a[q];
    }
    for (int q = l; q <= r; q++) {
        a[q] = c[q];
    }
}

int
main(void)
{
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d%d", &a[i].l, &a[i].r);
    }
    mergeSort(0, n - 1);
    int st = a[0].l, lt = a[0].r;
    for (int i = 1; i < n; i++) {
        if (lt < a[i].l) {
            printf("%d %d\n", st, lt);
            st = a[i].l;
            lt = a[i].r;
        } else {
            lt = max(lt, a[i].r);
        }
    }
    printf("%d %d", st, lt);
    return 0;
}
