#include <stdio.h>
#include <stdlib.h>
#include <math.h>

long long
result(void)
{
    char c;
    long long k;
    if (scanf("%lld", &k)) {
        return k;
    }
    scanf("%c", &c);
    if (c == '*') {
        return result() * result();
    }
    if (c == '/') {
        return result() / result();
    }
    return 1;
}

int
main(void)
{
    printf("%lld", result());
    return 0;
}
