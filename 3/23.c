#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct group {
    int n;
    int s;
} group;

int m, sign = 0;

enum {
    MAXSIZE = 20,
    MAX_S = 10000 * 200
};

group bl[20];

void result(int n, int s) {
    if (sign) {
        return;
    }
    if (!n) {
        int i;
        for (i = 0; i < m; i++) {
            if (bl[i].n) {
                break;
            }
        }
        if (i == m) {
            sign = 1;
        }
        return;
    }
    for (int i = 0; i < m; i++) {
        if (bl[i].n >= n && bl[i].s * n < s) {
            bl[i].n -= n;
            result(n - 1, bl[i].s * n);
            bl[i].n += n;
        }
    }
}

int
main(void)
{
    scanf("%d", &m);
    int n = 0;
    for (int i = 0; i < m; i++) {
        scanf("%d %d", &bl[i].s, &bl[i].n);
        n += bl[i].n;
    }
    for (int i = 1; i < 14; i++) {
        if (i * (i + 1) / 2 == n) {
            result(i, MAX_S);
            break;
        }
    }
    printf("%s", sign == 1 ? "Yes" : "No");
    return 0;
}
