#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define max(a, b) (a) > (b) ? (a) : (b)

enum {
    MAXSIZE = 14
};

int n, mx = 0;
int a[MAXSIZE];

void subset(int k, int sum1, int sum2) {
    if (sum1 == sum2) {
        mx = max(mx, sum2);
    }
    if (k == n) {
        return;
    }
    subset(k + 1, sum1 + a[k], sum2);
    subset(k + 1, sum1, sum2 + a[k]);
    subset(k + 1, sum1, sum2);
}

int
main(void)
{
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }
    subset(0, 0, 0);
    printf("%d", mx);
    return 0;
}
