#include <stdio.h>
#include <stdlib.h>
#include <math.h>

unsigned int reverse (unsigned int x) {
    unsigned int ans = 0;
    while (x) {
        ans *= 10;
        ans += x % 10;
        x /= 10;
    }
    return ans;
}

int
main(void)
{
    unsigned int x, m;
    scanf("%u%u", &x, &m);
    for (int i = 0; i < m; i++) {
        x += reverse(x);
    }
    if (x == reverse(x)) {
        printf("Yes\n%u", x);
    } else {
        printf("No");
    }
    return 0;
}
