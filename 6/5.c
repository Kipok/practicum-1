#include <stdio.h>
#include <stdlib.h>


int main(void)
{
    FILE *fr;
    if (!(fr = fopen("input.txt", "rt")))
        return 1;
    long long n = 0, ans = 0;
    int sign = 1;
    char c1 = '+', c;
    fscanf(fr, "%c", &c);
    while (sign) {
        n = 0;
        c1 = '+';
        if (c < '0' || c > '9')
            while (c < '0' || c > '9') {
                if (c == '-')
                    c1 = (c1 == '+') ? '-' : '+';
                if (fscanf(fr, "%c", &c) != 1) {
                    sign = 0;
                    break;
                }
            }
        while (c >= '0' && c <= '9') {
            n = n * 10 + (c - '0');
            if (fscanf(fr, "%c", &c) != 1) {
                sign = 0;
                break;
            }
        }
        ans += (c1 == '+') ? n : -n;
    }
    FILE *fp;
    if (!(fp = fopen("output.txt", "wt")))
        return 1;
    fprintf(fp, "%lld", ans);
    fclose(fp);
    fclose(fr);
    return 0;
}
