#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char uchar;

typedef union number {
    int x;
    uchar c[4];
} number;

void read(number *a, FILE *f) {
    fread(&a->c[3], sizeof(uchar), 1, f);
    fread(&a->c[2], sizeof(uchar), 1, f);
    fread(&a->c[1], sizeof(uchar), 1, f);
    fread(&a->c[0], sizeof(uchar), 1, f);
}

int main(void)
{
    FILE *fr;
    if (!(fr = fopen("matrix.in", "rb")))
        return 1;
    int n;
    uchar k;
    fread(&k, sizeof(uchar), 1, fr);
    n = k << 8;
    fread(&k, sizeof(uchar), 1, fr);
    n += k;
    long long ans = 0;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++) {
            number k;
            read(&k, fr);
            if (i == j)
                ans += k.x;
        }
    FILE *fp;
    if (!(fp = fopen("trace.out", "wb")))
        return 1;
    for (int i = 0; i < 8; i++) {
        uchar k = (ans & 0xFF00000000000000ll) >> 56;
        fwrite(&k, sizeof(uchar), 1, fp);
        ans <<= 8;
    }
    fclose(fp);
    fclose(fr);
    return 0;
}
