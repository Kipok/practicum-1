#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    cell_max = 105,
    str_max = 2105,
    line_max = 10505,
    row_max = 25
};

enum {
    STR, INT
};

typedef struct cell {
    union {
        char *s;
        int x;
    };
    char type;
} cell;

char s[str_max], s1[cell_max];
int n, m = 0;
cell table[line_max][row_max];

int isDigit(char c) {
    return c >= '0' && c <= '9';
}

void parse(char *s, int i) {
    int j = 0, t = 0, d = strlen(s);
    while (t < d && s[t] != '\n') {
        while (t < d && (s[t] == ' ' || s[t] == ';'))
            ++t;
        if (s[t] == '"') {
            int k = 0;
            ++t;
        //    printf("%d %d\n", i, j);
            table[i][j].type = STR;
            int q = t;
            while (q < d && s[q] != '"')
                ++q;
            table[i][j].s = (char *)calloc(q - t + 1, sizeof(char));
            while (t < d && s[t] != '"')
                table[i][j].s[k++] = s[t++];
            ++t;
        } else {
        //    printf("%d 응응응응응응응 %d\n", i, j);
            table[i][j].type = INT;
            table[i][j].x = 0;
            int sign = 0;
            if (s[t] == '-') {
                ++t;
                sign = 1;
            }
            while (t < d && isDigit(s[t]))
                table[i][j].x = table[i][j].x * 10 + (s[t++] - '0');
            if (sign)
                table[i][j].x = -table[i][j].x;
        }
        while (t < d && (s[t] == ' ' || s[t] == ';'))
            ++t;
        ++j;
    }
    m = j;
}

int comp(cell a, cell b) {
    if (a.type == INT)
        return a.x > b.x;
    return strcmp(a.s, b.s);
}

void swap (cell *a, cell *b) {
    cell k = *a;
    *a = *b;
    *b = k;
}

void swap_line(int a, int b) {
    for (int i = 0; i < m; i++)
        swap(&table[a][i], &table[b][i]);
}

void quick_sort(int l, int r) {
    cell x = table[(l + r) / 2][n];
    int i = l, j = r;
    while (i < j) {
        while (comp(x, table[i][n]) > 0)
            ++i;
        while (comp(table[j][n], x) > 0)
            --j;
        if (i <= j)
            swap_line(i++, j--);
    }
    if (i < r)
        quick_sort(i, r);
    if (l < j)
        quick_sort(l, j);
}

int main(void)
{
    FILE *fr;
    if (!(fr = fopen("input.txt", "rt")))
        return 1;
    int i = 0, k = 0;
    fscanf(fr, "%d", &n);
    fgets(s, str_max, fr);
    while (fgets(s, str_max, fr)) {
        strcat(s, "\n");
        parse(s, i++);
        ++k;
    }
    quick_sort(0, k - 1);
    FILE *fp;
    if (!(fp = fopen("output.txt", "wt")))
        return 1;
    for (int i = 0; i < k; i++) {
        for (int j = 0; j < m; j++) {
            if (table[i][j].type == INT)
                fprintf(fp, "%d", table[i][j].x);
            else {
                fprintf(fp, "\"%s\"", table[i][j].s);
                free(table[i][j].s);
            }
            if (j != m - 1)
                fprintf(fp, ";");
        }
        if (i != k - 1)
            fprintf(fp, "\n");
    }
    fclose(fp);/*
    for (int i = 0; i < 10000; i++) {
        for (int j = 0; j < 20; j++) {
            fprintf(fr, "\"");
            for (int t = 0; t < 100; t++) {
                fprintf(fr, "a");
            }
            fprintf(fr, "\"");
            if (j != 19)
                fprintf(fr, ";");
        }
        fprintf(fr, "\n");
    }*/
    fclose(fr);
    return 0;
}
