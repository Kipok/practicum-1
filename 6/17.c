#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int max(int a, int b)
{
    return a > b ? a : b;
}

typedef struct tree node;
typedef struct tree tree;

struct tree 
{
    node *l;
    node *r;
    int height;
    int key;
    int value;
};

node * create_node(int key, int value) 
{
    node * p = (node *) malloc(sizeof(node));
    p->key = key;
    p->value = value;
    p->l = p->r = NULL;
    p->height = 1;
    return p;
}

int get_height(node *p)
{
    if (!p)
        return 0;
    else
        return p->height;
}

int know_balance(node *p) 
{
    return get_height(p->r) - get_height(p->l);
}

void fix_height(node *p)
{
    p->height = max(get_height(p->r), get_height(p->l)) + 1;
}

node * small_right_rotation(node *p) 
{
    node * t = p->l;
    p->l = t->r;
    t->r = p;
    fix_height(t->r);
    fix_height(t);
    return t;
}

node * small_left_rotation(node *p) 
{
    node * t = p->r;
    p->r = t->l;
    t->l = p;
    fix_height(t->l);
    fix_height(t);
    return t;
}

node * big_right_rotation(node *p) 
{
    p->l = small_left_rotation(p->l);
    p = small_right_rotation(p);
    return p;
}

node * big_left_rotation(node *p) 
{
    p->r = small_right_rotation(p->r);
    p = small_left_rotation(p);
    return p;
}

node * balance_node (node *p) 
{
    fix_height(p);
    if (know_balance(p) == 2) {
        if (know_balance(p->r) > 0)
            p = small_left_rotation(p);
        else
            p = big_left_rotation(p);
    }
    if (know_balance(p) == -2) {
        if (know_balance(p->l) < 0)
            p = small_right_rotation(p);
        else
            p = big_right_rotation(p);
    }
    return p;
}

tree * add(tree *p, int key, int value) 
{
    if (!p)
        return p = create_node (key, value);
    if (key > p->key)
        p->r = add(p->r, key, value);
    if (key < p->key)
        p->l = add(p->l, key, value);
    if (key == p->key)
        p->value = value;
    return balance_node(p);
}

node * search(tree *p, int key) 
{
    if (!p || p->key == key)
        return p;
    if (key > p->key)
        return search(p->r, key);
    if (key < p->key)
        return search(p->l, key);
    return p;
}

node * find_min(node *p)
{
    if (p->l)
        return find_min(p->l);
    else
        return p;
}

node * remove_min(node *p)
{
    if (!p->l)
        return p->r;
    p->l = remove_min(p->l);
    return balance_node (p);
}

tree * del(tree *p, int key)
{
    if (!p)
        return p;
    if (key > p->key)
        p->r = del(p->r, key);
    if (key < p->key)
        p->l = del(p->l, key);
    if (key == p->key) {
        node *l = p->l;
        node *r = p->r;
        free(p);
        if(!r) 
            return l;
        node *mn = find_min(r);
        mn->r = remove_min(r);
        mn->l = l;
        return balance_node (mn);
    }
    return balance_node(p);
}

void free_tree(tree *p)
{
    if (!p)
        return;
    free_tree(p->l);
    free_tree(p->r);
    free(p);
}

tree *t;

int main(void) {
 //   freopen("input.txt", "rt", stdin);
  //  freopen("output.txt", "wt", stdout);
    while (1) {
        char c;
        scanf("%c", &c);
        if (c == 'F')
            break;
        if (c == 'A') {
            int k, v;
            scanf("%d %d\n", &k, &v);
            t = add(t, k, v);
        }
        if (c == 'S') {
            int k;
            scanf("%d\n", &k);
            node * p = search(t, k);
            if (p)
                printf("%d %d\n", p->key, p->value);
        }
        if (c == 'D') {
            int k;
            scanf("%d\n", &k);
            t = del(t, k);
        }
    }
    free_tree(t);
    return 0;
}

