#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node node;
typedef struct node list;

struct node {
    node *next;
    node *prev;
    int value;
};

list *l;
node **pr;

list* create_list(list *l, int n, int i, node *p) {
    if (!n)
        return NULL;
    l = (list *)malloc(sizeof(list));
    l->value = i;
    l->prev = p;
    l->next = create_list(l->next, n - 1, i + 1, l);
    pr[i] = l;
    return l;
}

list * transform_list(list *l, int a, int b) {
    if (l->next == pr[a])
        return l;
    pr[a]->prev->next = pr[b]->next;
    if (pr[b]->next)
        pr[b]->next->prev = pr[a]->prev;
    l->next->prev = pr[b];
    pr[b]->next = l->next;
    l->next = pr[a];
    pr[a]->prev = l;
    return l;
}

void print_list(list *l, FILE *fp) {
    l = l->next;
    while (l != NULL) {
        fprintf(fp, "%d ", l->value + 1);
        l = l->next;
    }
}

/*void pd(list *l) {
    l = l->next;
    while (l != NULL) {
        printf("%d ", l->value + 1);
        l = l->next;
    }
    printf("\n");
}*/

list* delete_list(list *l) {
    if (l == NULL) {
        free(pr);
        return l;
    }
    delete_list(l->next);
    free(l);
    return NULL;
}

int main(void)
{
    FILE *fr;
    if (!(fr = fopen("input.txt", "rt")))
        return 1;
    int n, m;
    fscanf(fr, "%d%d", &n, &m);
    pr = (node **)calloc(n, sizeof(node *));
    l = (list *)malloc(sizeof(list));
    l->next = create_list(l, n, 0, l);
    l->prev = NULL;
    l->value = -1;
    for (int i = 0; i < m; i++) {
        int a, b;
        fscanf(fr, "%d%d", &a, &b);
        l = transform_list(l, a - 1, b - 1);
       // pd(l);
    }
    FILE *fp;
    if (!(fp = fopen("output.txt", "wt")))
        return 1;
    print_list(l, fp);
    l = delete_list(l);
    fclose(fp);
    fclose(fr);
    return 0;
}