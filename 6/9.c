#include <stdio.h>
#include <stdlib.h>

int a[1000001];

int main(void)
{
    FILE *fr;
    if (!(fr = fopen("input.bin", "rb")))
        return 1;
    int i = 1;
    int sr = 1, sd = -1;
    while (fread(a + i, sizeof(int), 1, fr)) {
        if (i > 1) {
            if (a[i / 2] > a[i])
                sr = 0;
            if (a[i / 2] < a[i])
                sd = 0;
        }
        ++i;
    }
    FILE *fp;
    if (!(fp = fopen("output.bin", "wb")))
        return 1;
    if (sr)
        fwrite(&sr, sizeof(int), 1, fp);
    else
        fwrite(&sd, sizeof(int), 1, fp);
    fclose(fp);
    fclose(fr);
    return 0;
}
