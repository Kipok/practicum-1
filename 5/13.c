#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define inf 0x10007F

typedef struct pair {
    int x;
    int y;
} pair;

typedef struct heap heap;

typedef struct node {
    pair t;
    long long v;
} node;

struct heap {
    node v;
    heap *l;
    heap *r;
    heap *p;
};

heap *t;

void swap(heap **a, heap **b) {
    heap *q = *a;
    *a = *b;
    *b = q;
}

heap* merge(heap *a, heap *b) {
    if (!a || !b)
        return a ? a : b;
    if (b->v.v < a->v.v)
        swap(&a, &b);
    if (rand() & 1)
        swap(&a->l, &a->r);
    a->l = merge(a->l, b);
    a->l->p = a;
    return a;
}

pair get_min(heap **a) {
    pair x = (*a)->v.t;
    heap *b = merge((*a)->l, (*a)->r);
    free((*a));
    *a = b;
    if ((*a))
        (*a)->p = *a;
    return x;
}

void delete_node(heap **a) {
    heap *c = (*a)->p;
    heap *b = (heap *)malloc(sizeof(heap));
    b = merge((*a)->l, (*a)->r);
    free(*a);
    if (c != *a) {
        if (c->l == *a) {
            c->l = b;
            if (c->l)
                c->l->p = c;
        }
        else {
            c->r = b;
            if (c->r)
                c->r->p = c;
        }
    } else {
        t = b;
        if (t)
            t->p = t;
    }
}

heap* create_heap(node x) {
    heap *a = (heap *)malloc(sizeof(heap));
    a->v = x;
    a->l = a->r = NULL;
    a->p = a;
    return a;
}

void delete_heap(heap **a) {
    while (*a != NULL)
        get_min(&(*a));
}

int **h;
long long **a;
heap *(**u);
const int dx[4] = {1, -1, 0, 0};
const int dy[4] = {0, 0, 1, -1};

int initialising(int n, int m) {
    if (!(a = (long long **)malloc(n * sizeof(long long *))))
        return 1;
    if (!(h = (int **)malloc(n * sizeof(int *)))) {
        free(a);
        return 1;
    }
    if (!(u = (heap ***)malloc(n * sizeof(heap **)))) {
        free(a);
        free(h);
        return 1;
    }
    for (int i = 0; i < n; i++) {
        if (!(a[i] = (long long *)malloc(m * sizeof(long long))) || !(h[i] = (int *)malloc(m * sizeof(int))) || !(u[i] = (heap **)malloc(m * sizeof(heap *)))) {
            for (int j = 0; j < i; j++) {
                free(a[j]);
                free(h[j]);
                free(u[j]);
            }
            free(a);
            free(h);
            free(u);
            return 1;
        }
    }
    for (int i = 0; i < n; i++) {
        memset(a[i], inf, m * sizeof(long long));
        memset(u[i], 0, m * sizeof(heap *));
        for (int j = 0; j < m; j++) {
            scanf("%d", &h[i][j]);
        }
    }
    return 0;
}

void free_all(int n) {
    for (int i = 0; i < n; i++) {
        free(a[i]);
        free(h[i]);
        free(u[i]);
    }
    free(h);
    free(a);
    free(u);
    delete_heap(&t);
}

int equal(pair a, pair b) {
    return a.x == b.x && a.y == b.y;
}

int
main(void)
{
    srand(time(NULL));
    int n, m;
    pair st, end;
    scanf("%d%d%d%d%d%d", &n, &m, &st.x, &st.y, &end.x, &end.y);
    if (initialising(n, m))
        return 1;
    a[st.x][st.y] = 0;
    node x;
    x.t = st;
    x.v = 0;
    t = create_heap(x);
    while (t != NULL) {
        pair c = get_min(&t);
        if (equal(c, end)) {
            printf("%lld", a[end.x][end.y]);
            break;
        }
        for (int i = 0; i < 4; i++)
            if (c.x + dx[i] >= 0 && c.x + dx[i] < n && c.y + dy[i] >= 0 && c.y + dy[i] < m) {
                long long d = a[c.x][c.y] + abs(h[c.x + dx[i]][c.y + dy[i]] - h[c.x][c.y]);
                if (a[c.x + dx[i]][c.y + dy[i]] > d) {
                    a[c.x + dx[i]][c.y + dy[i]] = d;
                    x.v = d;
                    pair z = c;
                    z.x += dx[i];
                    z.y += dy[i];
                    x.t = z;
                    if (u[z.x][z.y])
                        delete_node(&u[z.x][z.y]);
                    u[z.x][z.y] = create_heap(x);
                    t = merge(u[z.x][z.y], t);
                }
            }
    }
    free_all(n);
    return 0;
}
