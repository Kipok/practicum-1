#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int *a;

int
main(void)
{
    a = (int *)malloc(4);
    int c = 0, sign = 1, sz = 1, i = 0;
    scanf("%d", &c);
    while (c != 0) {
        if (sign)
            printf("%d ", c);
        else
            a[i++] = c;
        sign ^= 1;
        if (i == sz) {
            sz *= 2;
            if (!(a = (int *)realloc(a, sz * sizeof(int)))) {
                free(a);
                return 1;
            }
        }
        scanf("%d", &c);
    }
    for (int j = 0; j < i; j++)
        printf("%d ", a[j]);
    free(a);
    return 0;
}
