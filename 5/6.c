#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *s;

int
main(void)
{
    int n;
    scanf("%d", &n);
    if (!(s = (char *)malloc(n + 1)))
        return -1;
    fgets(s, n + 1, stdin);
    fgets(s, n + 1, stdin);
    int i = 0, j = n - 1;
    while (i < j)
        if (s[i++] != s[j--]) {
            printf("NO");
            return 0;
        }
    printf("YES");
    free(s);
    return 0;
}
