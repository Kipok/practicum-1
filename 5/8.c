#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    inf = 1000
};

typedef struct pair {
    int x;
    int y;
} pair;

typedef struct node {
    pair v;
    struct node *n;
} node;

typedef struct queue {
    node *st;
    node *end;
    int size;
} queue;

queue o;

void
create_queue(void) {
    o.st = (node *)malloc(sizeof(node));
    o.end = (node *)malloc(sizeof(node));
    o.size = 0;
}

void
put(pair x) {
    node *r = (node *)malloc(sizeof(node));
    o.end->n = r;
    r->v = x;
    r->n = NULL;
    o.end = r;
    if (!o.size) {
        o.st = o.end;
        o.st->n = o.end;
    }
    ++o.size;
}

pair
get(void) {
    pair k = o.st->v;
    node *r = o.st->n;
    free(o.st);
    o.st = r;
    --o.size;
    return k;
}

void
delete_queue(void) {
    while (o.size)
        get();
}

int
equal(pair a, pair b) {
    return a.x == b.x && a.y == b.y;
}

int **a;
const int dx[8] = {1, 2, 2, 1, -1, -2, -2, -1};
const int dy[8] = {2, 1, -1, -2, -2, -1, 1, 2};

int
main(void)
{
    int n;
    pair st, end;
    scanf("%d%d%d%d%d", &n, &st.x, &st.y, &end.x, &end.y);
    --st.x;
    --st.y;
    --end.x;
    --end.y;
    if (!(a = (int **)malloc(n * sizeof(int *))))
        return 1;
    for (int i = 0; i < n; i++)
        if (!(a[i] = (int *)malloc(n * sizeof(int)))) {
            for (int j = 0; j < i; j++)
                free(a[j]);
            free(a);
            return 1;
        }
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            a[i][j] = inf;
    create_queue();
    a[st.x][st.y] = 0;
    put(st);
    while (o.size) {
        pair c = get();
        if (equal(c, end)) {
            printf("%d", a[end.x][end.y]);
            break;
        }
        for (int i = 0; i < 8; i++)
            if (c.x + dx[i] >= 0 && c.x + dx[i] < n && c.y + dy[i] >= 0 && c.y + dy[i] < n && a[c.x + dx[i]][c.y + dy[i]] > a[c.x][c.y] + 1) {
                pair k;
                k.x = c.x + dx[i];
                k.y = c.y + dy[i];
                a[k.x][k.y] = a[c.x][c.y] + 1;
                put(k);
            }
    }
    if (a[end.x][end.y] == 1000)
        printf("-1");
    delete_queue();
    for (int i = 0; i < n; i++)
        free(a[i]);
    free(a);
    return 0;
}
