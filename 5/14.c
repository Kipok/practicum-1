#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int **a;
int n, buf = 1, c = 0;

const int d[4] = {1, 3, 7, 9};

int isPrime(int x) {
    for (int i = 2; i * i <= x; i++)
        if (x % i == 0)
            return 0;
    return 1;
}

void put(int x, int k) {
    if (!c) {
        buf = 1;
        a[k] = (int *) malloc(buf * sizeof(int));
    }
    if (c == buf)
        a[k] = (int *)realloc(a[k], (buf *= 2) * sizeof(int));
    a[k][c++] = x;
}

void gen(int k) {
    if (k == n)
        return;
    int i = 0;
    c = 0;
    while (a[k - 1][i] != -1) {
        for (int j = 0; j < 4; j++) {
            if (isPrime(a[k - 1][i] * 10 + d[j]))
                put(a[k - 1][i] * 10 + d[j], k);
        }
        ++i;
    }
    put(-1, k);
    gen(k + 1);
}

int
main(void)
{
    scanf("%d", &n);
    a = (int **)calloc(n, sizeof(int *));
    a[0] = (int *)calloc(5, sizeof(int));
    a[0][0] = 2;
    a[0][1] = 3;
    a[0][2] = 5;
    a[0][3] = 7;
    a[0][4] = -1;
    gen(1);
    int i = 0;
    while (a[n - 1][i] != -1)
        printf("%d ", a[n - 1][i++]);
    for (i = 0; i < n; i++)
        free(a[i]);
    free(a);
    return 0;
}
