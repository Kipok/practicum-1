#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define max(a, b) (a) > (b) ? (a) : (b)

enum {
    SIZE = 20005
};

typedef struct BigInt {
    int *data;
    int size;
    int neg;
} BigInt;

char s[SIZE];
BigInt a, b, c;

void
print(BigInt a) {
    if (a.neg)
        printf("-");
    for (int i = a.size - 1; i >= 0; i--)
        printf("%d", a.data[i]);
}

BigInt
sum(BigInt a, BigInt b) {
    c.size = max(a.size, b.size);
    c.neg = 0;
    c.data = (int *)calloc(c.size + 1, sizeof(int));
    memset(c.data, 0, sizeof(c.data));
    for (int i = 0; i < c.size; i++) {
        c.data[i + 1] = (c.data[i] + a.data[i] + b.data[i]) / 10;
        c.data[i] = (c.data[i] + a.data[i] + b.data[i]) % 10;
    }
    if (c.data[c.size])
        ++c.size;
    return c;
}

int
comp(BigInt a, BigInt b) {
    if (a.size != b.size)
        return a.size < b.size;
    for (int i = a.size - 1; i >= 0; i--)
        if (a.data[i] != b.data[i])
            return a.data[i] < b.data[i];
    return 0;
}

void
swap(BigInt *a, BigInt *b) {
    BigInt t = *a;
    *a = *b;
    *b = t;
}

BigInt
subtract(BigInt a, BigInt b) {
    c.size = max(a.size, b.size);
    c.neg = 0;
    c.data = (int *)calloc(c.size, sizeof(int));
    memset(c.data, 0, sizeof(c.data));
    if (comp(a, b)) {
        c.neg = 1;
        swap(&a, &b);
    }
    for (int i = 0; i < c.size; i++) {
        if (a.data[i] >= b.data[i])
            c.data[i] = a.data[i] - b.data[i];
        else {
            c.data[i] = a.data[i] + 10 - b.data[i];
            ++i;
            while (a.data[i] - 1 < b.data[i]) {
                c.data[i] = a.data[i] + 9 - b.data[i];
                ++i;
            }
            c.data[i] = a.data[i] - 1 - b.data[i];
        }
    }
    while (c.size > 1 && c.data[c.size - 1] == 0)
        --c.size;
    return c;
}

BigInt
multiply(BigInt a, BigInt b) {
    c.size = a.size + b.size;
    c.neg = 0;
    c.data = (int *)calloc(c.size + 1, sizeof(int));
    memset(c.data, 0, sizeof(c.data));
    for (int i = 0; i < a.size; i++)
        for (int j = 0; j < b.size; j++)
            c.data[i + j] += a.data[i] * b.data[j];
    for (int i = 0; i < c.size; i++) {
        c.data[i + 1] += c.data[i] / 10;
        c.data[i] = c.data[i] % 10;
    }
    while (c.size > 1 && c.data[c.size - 1] == 0)
        --c.size;
    return c;
}

int
main(void)
{
    fgets(s, SIZE, stdin);
    int d = strlen(s);
    char ch;
    int i = 0;
    if (!(a.data = (int *)calloc(d, sizeof(int))))
        return 1;
    memset(a.data, 0, sizeof(a.data));
    a.size = 0;
    if (!(b.data = (int *)calloc(d, sizeof(int)))) {
        free(a.data);
        return 1;
    }
    memset(b.data, 0, sizeof(b.data));
    b.size = 0;
    i = d - 1;
    while (s[i] != '-' && s[i] != '*' && s[i] != '+') {
        if (s[i] >= '0' && s[i] <= '9')
            b.data[b.size++] = s[i] - '0';
        --i;
    }
    ch = s[i--];
    while (i >= 0)
        a.data[a.size++] = s[i--] - '0';
    switch(ch) {
        case '+':
            print(sum(a, b));
            break;
        case '*':
            print(multiply(a, b));
            break;
        case '-':
            print(subtract(a, b));
            break;
        default:
            break;
    }
    free(a.data);
    free(b.data);
    free(c.data);
    return 0;
}
