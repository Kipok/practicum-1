#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    SIZE = 505
};

long long a[SIZE][SIZE];
int n;

long long gcd(long long a, long long b) {
    if (!b)
        return a;
    return gcd(b, a % b);
}

void swap(long long *a, long long *b) {
    long long t = *a;
    *a = *b;
    *b = t;
}

void swap_line(int c, int d) {
    for (int i = 0; i <= n; i++)
        swap(&a[c][i], &a[d][i]);
}

void multiply_line(long long x, int i, int j) {
    for (; j <= n; j++)
        a[i][j] *= x;
}

void normalise(int x, int i) {
    int d = a[x][i++];
    for (; i <= n; i++) {
        d = gcd(d, a[x][i]);
        if (abs(d) == 1)
            return;
    }
    for (int i = 0; i <= n; i++)
        a[x][i] /= d;
}

void subtract_line(long long x, int j, int i, int k) {
    for (; k <= n; k++)
        a[j][k] -= a[i][k] * x;
}

void transform(void) {
    for (int i = 0; i < n; i++) {
        if (!a[i][i]) {
            for (int j = i + 1; j < n; j++) {
                if (a[j][i]) {
                    swap_line(i, j);
                    break;
                }
            }
        }
        normalise(i, i);
        for (int j = i + 1; j < n; j++) {
            if (a[j][i]) {
                multiply_line(a[i][i], j, i);
                subtract_line(a[j][i] / a[i][i], j, i, i);
                normalise(j, i);
            }
        }
    }
}

void make_diogonal(void) {
    for (int i = n - 1; i >= 0; i--) {
        normalise(i, i);
        for (int j = 0; j < i; j++) {
            if (a[j][i]) {
                multiply_line(a[i][i], j, 0);
                subtract_line(a[j][i] / a[i][i], j, i, i);
                normalise(j, j);
            }
        }
    }
}

int
main(void)
{
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j <= n; j++)
            scanf("%lld", &a[i][j]);
    }
    transform();
    make_diogonal();
    for (int i = 0; i < n; i++) {
        printf("%lld\n", a[i][n] / a[i][i]);
    }
    return 0;
}
