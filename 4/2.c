#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    S_SIZE = 205,
    I_SIZE = 20005
};

char str[S_SIZE];
int a[I_SIZE];
const char vowel[12] = {'a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y'};

int isVowel(char c) {
    for (int j = 0; j < 12; j++)
        if (vowel[j] == c)
            return 1;
    return 0;
}

int
valid(char *s, int n)
{
    int d = strlen(s);
    for (int i = 0; i < d; i++) {
        if (isVowel(s[i]) && (!i || !isVowel(s[i - 1]))) {
            --n;
        }
    }
    return n == 0;
}

int
main(void)
{
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }
    fgets(str, S_SIZE, stdin);
    for (int i = 0; i < n; i++) {
        fgets(str, S_SIZE, stdin);
        if (valid(str, a[i])) {
            printf("%s", str);
        }
    }
    return 0;
}
