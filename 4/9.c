#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    S_SIZE = 2000000,
};

char s[S_SIZE], buf[S_SIZE];
int k, wc = 0;

void
print(void)
{
    buf[strlen(buf) - 1] = '\0';
    int diff = k - strlen(buf);
    if (wc == 1) {
        printf("%s", buf);
        for (int i = 0; i < diff; i++)
            putc(' ', stdout);
        putc('\n', stdout);
        return;
    }
    int d = strlen(buf), ch = diff / (wc - 1), r = diff % (wc - 1);
    for (int i = 0; i < d; i++) {
        putc(buf[i], stdout);
        if (buf[i] == ' ') {
            for (int j = 0; j < ch; j++)
                putc(' ', stdout);
            if (r) {
                putc(' ', stdout);
                --r;
            }
        }
    }
    putc('\n', stdout);
}

int
main(void)
{
    scanf("%d", &k);
    fgets(s, S_SIZE, stdin);
    while (scanf("%s", s) != -1) {
        if (strlen(buf) + strlen(s) > k) {
            print();
            strcpy(buf, "");
            wc = 0;
        }
        ++wc;
        strcat(buf, s);
        strcat(buf, " ");
    }
    if (strcmp(buf, ""))
        print();
    return 0;
}
