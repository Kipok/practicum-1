#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    S_SIZE = 1000010
};

char s1[S_SIZE], s2[S_SIZE], s[2 * S_SIZE];
int p[2 * S_SIZE];

void pr_func(int n) {
    memset(p, 0, sizeof(p));
    for (int i = 1; i <= n; i++) {
        int j = p[i - 1];
        while (j && s[i] != s[j]) {
            j = p[j - 1];
        }
        if (s[i] == s[j])
            p[i] = j + 1;
        else
            p[i] = j;
    }
}

int
main(void)
{
    fgets(s1, S_SIZE, stdin);
    s1[strlen(s1) - 1] = '\0';
    fgets(s2, S_SIZE, stdin);
    s2[strlen(s2) - 1] = '\0';
    int d1 = strlen(s1), d2 = strlen(s2);
    strcpy(s, s1);
    strcat(s, " ");
    strcat(s, s2);
    pr_func(d1 + d2);
    printf("%d ", p[d1 + d2]);
    strcpy(s, s2);
    strcat(s, " ");
    strcat(s, s1);
    pr_func(d1 + d2);
    printf("%d", p[d1 + d2]);
    return 0;
}
