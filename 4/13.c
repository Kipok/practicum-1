#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    S_SIZE = 1000010
};

char s[S_SIZE];
int p[S_SIZE];

void pr_func(int n) {
    memset(p, 0, sizeof(p));
    for (int i = 1; i < n; i++) {
        int j = p[i - 1];
        while (j && s[i] != s[j]) {
            j = p[j - 1];
        }
        if (s[i] == s[j])
            p[i] = j + 1;
        else
            p[i] = j;
    }
}

int
main(void)
{
    fgets(s, S_SIZE, stdin);
    if (s[strlen(s) - 1] == '\n')
        s[strlen(s) - 1] = '\0';
    int n = strlen(s);
    pr_func(n);
    int d = p[n - 1];
    while (d) {
        printf("%d ", n - d);
        d = p[d - 1];
    }
    printf("%d", n);
    return 0;
}