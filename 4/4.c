#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    S_SIZE = 260
};

char s[S_SIZE];

int
main(void)
{
    fgets(s, S_SIZE, stdin);
    int d = strlen(s), count_e = 0, count_o = 0 , curr_o = 0, curr_e = 0;
    for (int j = 0; j < d; j++) {
        if (j & 1) {
            curr_e += (s[j] == 'A');
        } else {
            curr_o += (s[j] == 'A');
        }
    }
    for (int i = 0; i < d - 1; i++) {
        if (i & 1) { // j % 2
            curr_e -= (s[i] == 'A');
        } else {
            curr_o -= (s[i] == 'A');
        }
        if (count_o + curr_o == count_e + curr_e) {
            printf("%d ", i + 1);
        }
        if (i & 1) {
            count_o += (s[i] == 'A');
        } else {
            count_e += (s[i] == 'A');
        }
    }
    return 0;
}
