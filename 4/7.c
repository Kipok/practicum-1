#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define max(x, y) ((x) > (y)) ? (x) : (y)

int a[101], used[101], max_l = 0, n;
char s[15][30];

void length(int x, int t) {
    max_l = max(max_l, t);
    for (int i = 0; i < n; i++) {
        if (!used[i]) {
            if (s[x][strlen(s[x]) - 1] == s[i][0]) {
                used[i] = 1;
                length(i, t + 1);
                used[i] = 0;
            }
        }
    }
}

int
main(void)
{
    scanf("%d\n", &n);
    for (int i = 0; i < n; i++) {
        fgets(s[i], 30, stdin);
        if (s[i][strlen(s[i]) - 1] == '\n')
            s[i][strlen(s[i]) - 1] = '\0';
    }
    int mx = 0;
    for (int i = 0; i < n; i++) {
        used[i] = 1;
        max_l = 0;
        length(i, 1);
        a[i] = max_l;
        mx = max(a[i], mx);
        used[i] = 0;
    }
    printf("%d\n", mx);
    for (int i = 0; i < n; i++) {
        if (a[i] == mx) {
            printf("%s\n", s[i]);
        }
    }
    return 0;
}
