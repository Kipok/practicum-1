#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    unsigned int a11, a12, a21, a22, b1, b2, x = 0, y = 0, nul = 0;
    scanf("%u%u%u%u%u%u", &a11, &a12, &a21, &a22, &b1, &b2);
    for (unsigned int i = 0; i < 32; i++) {
        nul += 1U << i;
        if (((a11 & x) ^ (a12 & y)) == (b1 & nul)) {
            if (((a21 & x) ^ (a22 & y)) == (b2 & nul)) {
                continue;
            }
        }
        if (((a11 & (x + (1U << i))) ^ (a12 & y)) == (b1 & nul)) {
            if (((a21 & (x + (1U << i))) ^ (a22 & y)) == (b2 & nul)) {
                x += (1U << i);
                continue;
            }
        }
        if (((a11 & x) ^ (a12 & (y + (1U << i)))) == (b1 & nul)) {
            if (((a21 & x) ^ (a22 & (y + (1U << i)))) == (b2 & nul)) {
                y += (1U << i);
                continue;
            }
        }
        if (((a11 & (x + (1U << i))) ^ (a12 & (y + (1U << i)))) == (b1 & nul)) {
            if (((a21 & (x + (1U << i))) ^ (a22 & (y + (1U << i)))) == (b2 & nul)) {
                x += (1U << i);
                y += (1U << i);
                continue;
            }
        }
        printf("No");
        return 0;
    }
    printf("Yes\n%u %u", x, y);
    return 0;
}
