#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    long long n, a, p1 = 0, p2 = 0, n1 = 0, n2 = 0, z = 0;
    scanf("%lld", &n);
    for (int i = 0; i < n; i++) {
        scanf("%lld", &a);
        if (a > p2) {
            if (a > p1) {
                p2 = p1;
                p1 = a;
            } else {
                p2 = a;
            }
        }
        if (a < n2) {
            if (a < n1) {
                n2 = n1;
                n1 = a;
            } else {
                n2 = a;
            }
        }
        if (a == 0) {
            ++z;
        }
    }
    if (p1 && p2) {
        if (n1 && n2) {
            if (p1 * p2 <= n1 * n2) {
                printf("%lld %lld", n1, n2);
            } else {
                printf("%lld %lld", p2, p1);
            }
        } else {
            printf("%lld %lld", p2, p1);
        }
    } else {
        if (n1 && n2) {
            printf("%lld %lld", n1, n2);
        } else {
            if (z > 0) {
                if (n1) {
                    printf("%lld 0", n1);
                } else {
                    if (z > 1) {
                        printf("0 0");
                    } else {
                        printf("0 %lld", p1);
                    }
                }
            } else {
                printf("%lld %lld", n1, p1);
            }
        }
    }
    return 0;
}
