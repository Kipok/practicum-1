#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    unsigned int x = 0, a, b, c, d;
    scanf("%u%u%u%u", &a, &b, &c, &d);
    x = a + (b << 8) + (c << 16) + (d << 24);
    printf("%u", x);
    return 0;
}
