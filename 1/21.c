#include <stdio.h>
#include <stdlib.h>

enum {
    TONN = 1000 * 1000
};

int
main(void)
{
    int n, k = 1, curr = 1, size1 = 0, size2 = 0;
    scanf("%d", &n);
    size2 += n;
    while (n > 1) {
        if (n % 3 == 1) {
            --n;
            ++k;
            size1 += curr;
        }
        if (n % 3 == 2) {
            ++n;
            ++k;
            size2 += curr;
        }
        curr *= 3;
        n /= 3;
    }
    if (size1 > TONN || size2 > TONN) {
        printf("-1");
    }
    else {
        printf("%d", k);
    }
    return 0;
}
