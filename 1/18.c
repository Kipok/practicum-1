#include <stdio.h>
#include <stdlib.h>

int get(int x, int k) {
    return (x & (1 << k)) >= 1;
}

const int m[16] = {0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15};

int
main(void)
{
    int a, b = 0;
    scanf("%x", &a);
    for (int i = 0; i < 16; i++) {
        b += get(a, m[i]) << i;
    }
    if (b == 0) {
        printf("0000");
        return 0;
    } else if (b < 16) {
        printf("000");
    } else if (b < 256) {
        printf("00");
    } else if (b < 4096)
        printf("0");
    printf("%x", b);
    return 0;
}
