#include <stdio.h>
#include <stdlib.h>

int
main (void)
{
    int n = 0;
    char c;
    while ((c = getchar()) != '.') {
        if (c >= '0' && c <= '9') {
            n *= 10;
            n += c - '0';
        }
        else {
            if (c == ' ')
                printf(" ");
            else
                if (c > 'Z')
                    printf("%c", (((c - 'a' + n) % 26) + 'a'));
                else
                    printf("%c", (((c - 'A' + n) % 26) + 'A'));
        }
    }
    printf(".");
    return 0;
}
