#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    int n;
    double x;
    scanf("%lf%d", &x, &n);
    double ans = 0.0, curr = x, fact = 1, ci = 2;
    for (int i = 0; i < n; i++) {
        if (i & 1) {
            ans -= curr / fact;
        } else {
            ans += curr / fact;
        }
        fact *= ci * (ci + 1);
        ci += 2;
        curr *= x * x;
    }
    printf("%.6lf", ans);
    return 0;
}
