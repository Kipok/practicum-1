#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    long long n, k;
    scanf("%lld%lld", &n, &k);
    printf("%lld", ((n % (1 << k)) << (32 - k)) + (n >> k));
    return 0;
}
