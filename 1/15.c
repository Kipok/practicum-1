#include <stdio.h>
#include <stdlib.h>

int area(int x1, int y1, int x2, int y2, int x3, int y3)
{
    return ((x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1)) < 0;
}

int
main(void)
{
    int n, x1, y1, x2, y2, x3, y3, sign = 0, x_1, y_1, x_2, y_2;
    scanf("%d%d%d%d%d%d%d", &n, &x1, &y1, &x2, &y2, &x3, &y3);
    x_1 = x1;
    y_1 = y1;
    x_2 = x2;
    y_2 = y2;
    sign = area(x1, y1, x2, y2, x3, y3);
    for (int i = 3; i < n; i++) {
        if (sign != area(x1, y1, x2, y2, x3, y3)) {
            printf("No");
            return 0;
        }
        x1 = x2;
        y1 = y2;
        x2 = x3;
        y2 = y3;
        scanf("%d%d", &x3, &y3);
    }
    if (sign != area(x2, y2, x3, y3, x_1, y_1) || sign != area(x3, y3, x_1, y_1, x_2, y_2)) {
        printf("No");
        return 0;
    }
    printf("Yes");
    return 0;
}
