#include <stdio.h>
#include <stdlib.h>

#define min(a, b) a < b ? a : b

int distance (int dif) {
    return dif * 2 - ((dif & 1) ? 1 : 0);
}

int
main(void)
{
    int x, y, mn1 = 0, mn2 = 0, dif = 0;
    scanf("%d%d", &x, &y);
    x = abs(x);
    y = abs(y);
    if (x - 1 < y) {
        dif = y - (x - 1);
        mn1 = 2 * x - 1 + distance(dif);
    } else {
        dif = x - y;
        mn1 = 2 * y + distance(dif);
    }
    if (y - 1 < x) {
        dif = x - (y - 1);
        mn2 = 2 * y - 1 + distance(dif);
    } else {
        dif = y - x;
        mn2 = 2 * x + distance(dif);
    }
    printf("%d", min(mn1, mn2));
    return 0;
}
