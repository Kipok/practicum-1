#include <stdio.h>

int
main(void)
{
    int n, a, b, num = 1, ans = 1;
    scanf("%d%d", &n, &a);
    for (int i = 1; i < n; i++) {
        scanf("%d", &b);
        if (a < b)
            ++num;
        else
            num = 1;
        ans = ans > num ? ans : num;
        a = b;
    }
    printf("%d", ans);
    return 0;
}

