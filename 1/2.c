#include <stdio.h>
#include <stdlib.h>

int
main (void)
{
    long long n, a, mx, num = 1;
    scanf("%lld%lld", &n, &mx);
    for (int i = 1; i < n; i++) {
        scanf("%lld", &a);
        if (a == mx)
            ++num;
        if (a > mx) {
            mx = a;
            num = 1;
        }
    }
    printf("%lld", num);
    return 0;
}
