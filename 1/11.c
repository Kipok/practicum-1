#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    unsigned int n;
    scanf("%u", &n);
    printf("%u", n ^ (255 << 24));
    return 0;
}
