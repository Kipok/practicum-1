#include <stdio.h>
#include <stdlib.h>

int
main (void)
{
    int n, a, mx = -1e9, mn = 1e9;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", &a);
        if (a > mx)
            mx = a;
        if (a < mn)
            mn = a;
    }
    printf("%d", mx - mn);
    return 0;
}
