#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    double x1, y1, x2, y2, x3, y3;
    scanf("%lf%lf%lf%lf%lf%lf", &x1, &y1, &x2, &y2, &x3, &y3);
    printf("%.4lf %.4lf", (x2 + x3 + x1) / 3, (y2 + y3 + y1) / 3);
    return 0;
}
