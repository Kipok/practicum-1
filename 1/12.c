#include <stdio.h>
#include <stdlib.h>

#define swap(a, b) a ^= b, b ^= a, a ^= b

int
main(void)
{
    int n, a, b, c, x;
    scanf("%d%d%d%d", &n, &a, &b, &c);
    if (a <= b) {
        swap(a, b);
    }
    if (a <= c) {
        swap(a, c);
    }
    if (b <= c) {
        swap(b, c);
    }
    for (int i = 3; i < n; i++) {
        scanf("%d", &x);
        if (x > a) {
            c = b;
            b = a;
            a = x;
        } else if (x > b) {
            c = b;
            b = x;
        } else if (x > c) {
            c = x;
        }
    }
    printf("%d %d %d", a, b, c);
    return 0;
}
