#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    int n, ans, a;
    scanf("%d%d", &n, &ans);
    for (int i = 1; i < n; i++) {
        scanf("%d", &a);
        ans ^= a;
    }
    printf("%d", ans);
    return 0;
}
