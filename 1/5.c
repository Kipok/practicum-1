#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    int a, ans = 0, prev = 1, sign = 0;
    scanf("%d", &a);
    while (a != 0) {
        if (!sign) {
            prev *= a;
            ans += prev;
        }
        else
            prev = a;
        sign ^= 1;
        scanf("%d", &a);
    }
    if (!sign)
        ans += prev;
    printf("%d", ans);
    return 0;
}

