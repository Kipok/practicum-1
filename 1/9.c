#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    long long n, ans = 0;
    scanf("%lld", &n);
    for (long long i = 0; i <= 32; i++) {
        ans += (n & (1ll << i)) == (1ll << i);
    }
    printf("%lld", ans);
    return 0;
}
