#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    long long n, k, curr = 0, mx = 0;
    scanf("%lld%lld", &n, &k);
    for (int i = 0; i <= 32 - k; i++) {
        curr = n % (1 << k);
        n >>= 1;
        mx = mx > curr ? mx : curr;
    }
    printf("%lld", mx);
    return 0;
}
