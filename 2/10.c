#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double dAbs(double x) {
    return x > 1e-7 ? x : -x;
}

int
main(void)
{
    int n;
    scanf("%d", &n);
    double s[n], a[n];
    for (int i = 0; i < n; i++) {
        scanf("%lf", &s[i]);
    }
    for (int i = 0; i < n; i++) {
        scanf("%lf", &a[i]);
    }
    double v = 0, t = 0, v1 = 0;
    for (int i = 0; i < n; i++) {
        v1 = sqrt(2.0 * a[i] * s[i] + v * v);
        if (dAbs(a[i]) > 1e-7) {
            t += (v1 - v) / a[i];
        } else {
            if (v > 1e-7) {
                t += s[i] / v;
            }
        }
        v = v1;
    }
    printf("%.4lf", t);
    return 0;
}
