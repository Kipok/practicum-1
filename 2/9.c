#include <stdio.h>
#include <stdlib.h>

int
main(void)
{
    int n, m;
    scanf("%d", &n);
    int a[n];
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }
    scanf("%d", &m);
    int b[m], ans[m];
    for (int i = 0; i < m; i++) {
        scanf("%d", &b[i]);
    }
    int currx = 1;
    for (int i = 0; i < m; i++) {
        ans[i] = 0;
        currx = 1;
        for (int j = 0; j < n; j++) {
            ans[i] += a[j] * currx;
            currx *= b[i];
        }
    }
    for (int i = m - 1; i >= 0; i--) {
        printf("%d ", ans[i]);
    }
    return 0;
}
