#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

char s[150];
int a[10][10];
const int dx[9] = {0, 1, 0, -1, 0, 1, -1, 1, -1};
const int dy[9] = {0, 0, 1, 0, -1, 1, -1, -1, 1};

int
main(void)
{
    gets(s);
    int k = 0;
    for (int i = 0; i < strlen(s); i++) {
        if (i & 1) {
            a[k][s[i] - '0'] = 1;
        } else {
            k = s[i] - 'a' + 1;
        }
    }
    int sign = 0, ans = 0;
    for (int i = 1; i <= 8; i++) {
        for (int j = 1; j <= 8; j++) {
            sign = 0;
            for (int t = 0; t < 9; t++) {
                if (a[i + dy[t]][j + dx[t]]) {
                    sign = 1;
                    break;
                }
            }
            if (!sign) {
                ++ans;
            }
        }
    }
    printf("%d", ans);
    return 0;
}
