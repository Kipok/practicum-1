#include <stdio.h>
#include <stdlib.h>

#define max(n, m) n > m ? n : m
#define sqr(x) (x) * (x)

enum {
    MAXSIZE = 100001
};

int a[MAXSIZE], b[MAXSIZE];

int
main(void)
{
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
        if (a[i] <= 0) {
            printf("No");
            return 0;
        }
        ++b[a[i]];
    }
    for (int i = 1; i <= n; i++) {
        if (b[i] != 1) {
            printf("No");
            return 0;
        }
    }
    printf("Yes");
    return 0;
}
