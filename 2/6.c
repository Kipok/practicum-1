#include <stdio.h>
#include <stdlib.h>

#define max(n, m) n > m ? n : m
#define sqr(x) (x) * (x)

enum {
    MAXSIZE = 100001
};

int a[MAXSIZE], b[MAXSIZE];

int
main(void)
{
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }
    for (int i = 0; i < n; i++) {
        b[i] = a[a[i] - 1];
    }
    for (int i = 0; i < n; i++) {
        printf("%d ", a[b[i] - 1]);
    }
    return 0;
}
