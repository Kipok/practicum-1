#include <stdio.h>
#include <stdlib.h>

int a[1000001];

int
main(void)
{
    int n = 0;
    scanf("%d", &a[n]);
    while (a[n] != 0) {
        scanf("%d", &a[++n]);
    }
    for (int i = 0; i < n; i++) {
        if (!(i & 1)) {
            printf("%d ", a[i]);
        }
    }
    for (int i = n - 1; i >= 0; i--) {
        if (i & 1) {
            printf("%d ", a[i]);
        }
    }
    return 0;
}
