#include <stdio.h>
#include <stdlib.h>

#define max(n, m) n > m ? n : m

enum {
    MAXSIZE = 1000001
};

int a[MAXSIZE], b[MAXSIZE];

int
main(void)
{
    int n, m;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }
    scanf("%d", &m);
    for (int i = 0; i < m; i++) {
        scanf("%d", &b[i]);
    }
    int i = 0, j = 0;
    while (i < n && j < m) {
        if ((i + j) % 2 == 0) {
            printf("%d ", a[i++]);
        } else {
            printf("%d ", b[j++]);
        }
    }
    for (int t = i; t < n; t++) {
        printf("%d ", a[t]);
    }
    for (int t = j; t < m; t++) {
        printf("%d ", b[t]);
    }
    return 0;
}
