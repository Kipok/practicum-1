#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define swap(a, b) a ^= b, b ^= a, a ^= b

enum {
    MAXSIZE = 1500001,
    p = 1100101
};

int a[MAXSIZE], h[p][10], ans[3];

int
main(void)
{
    int n;
    scanf("%d", &n);
    memset(h, -1, sizeof(h));
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
        int j = 0;
        while (h[a[i] % p][j] != -1) {
            if (h[a[i] % p][j++] == a[i]) {
                while (h[a[i] % p][j] != -1) {
                    h[a[i] % p][j - 1] = h[a[i] % p][j];
                    ++j;
                }
                h[a[i] % p][j - 1] = -1;
                j = -1;
                break;
            }
        }
        if (j != -1) {
            h[a[i] % p][j] = a[i];
        }
    }
    int t = 0;
    for (int i = 0; i < p; i++) {
        int j = 0;
        while (h[i][j] != -1) {
            ans[t++] = h[i][j++];
        }
    }
    if (ans[0] > ans[1])
        swap(ans[0], ans[1]);
    if (ans[0] > ans[2])
        swap(ans[0], ans[2]);
    if (ans[1] > ans[2])
        swap(ans[1], ans[2]);
    printf("%d %d %d", ans[0], ans[1], ans[2]);
    return 0;
}
