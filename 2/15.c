#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define swap(a, b) a ^= b, b ^= a, a ^= b
#define min(a, b) (a) < (b) ? (a) : (b)
#define max(a, b) (a) > (b) ? (a) : (b)

enum {
    MAXSIZE = 1500001
};

int a[MAXSIZE];

int
main(void)
{
    int n, x = 0, a1 = 0, a2 = 0;
    scanf("%d", &n);
    scanf("%d", &a[0]);
    x = a[0];
    for (int i = 1; i < n; i++) {
        scanf("%d", &a[i]);
        x ^= a[i];
    }
    int j = 0;
    while (!(x & (1 << j))) {
        ++j;
    }
    int sign1 = 0, sign2 = 0;
    for (int i = 0; i < n; i++) {
        if (a[i] & (1 << j)) {
            if (!sign1) {
                a1 = a[i];
                sign1 = 1;
            } else {
                a1 ^= a[i];
            }
        } else {
            if (!sign2) {
                sign2 = 1;
                a2 = a[i];
            } else {
                a2 ^= a[i];
            }
        }
    }
    printf("%d %d", min(a1, a2), max(a1, a2));
    return 0;
}
