#include <stdio.h>
#include <stdlib.h>

#define max(n, m) n > m ? n : m

enum {
    MAXSIZE = 1000001
};

int a[MAXSIZE], b[MAXSIZE];

int
main(void)
{
    int n, ans = 0;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }
    for (int i = 0; i < n; i++) {
        scanf("%d", &b[i]);
    }
    for (int i = 0; i < n; i++) {
        ans += max(a[i], b[i]);
    }
    printf("%d", ans);
    return 0;
}
