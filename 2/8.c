#include <stdio.h>
#include <stdlib.h>

#define max(n, m) n > m ? n : m
#define sqr(x) (x) * (x)

enum {
    MAXSIZE = 100001
};

int a[MAXSIZE];

int
main(void)
{
    int n, ans = 0;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }
    for (int i = 0; i < n; i++) {
        for (int j = i + 2; j < n; j += 2) {
            if ((a[i] + a[j]) % 2 == 0 && (a[i] + a[j]) / 2 == a[(i + j) / 2]) {
                ++ans;
            }
        }
    }
    printf("%d", ans);
    return 0;
}
