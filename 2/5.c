#include <stdio.h>
#include <stdlib.h>

#define max(n, m) n > m ? n : m
#define sqr(x) (x) * (x)

enum {
    MAXSIZE = 1001
};

struct ball {
    long double x;
    long double y;
    long double z;
    long double r;
};

struct ball a[MAXSIZE];

long double l(struct ball t1, struct ball t2) {
    return sqr(t1.x - t2.x) + sqr(t1.y - t2.y) + sqr(t1.z - t2.z);
}

int
main(void)
{
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%Lf%Lf%Lf%Lf", &a[i].x, &a[i].y, &a[i].z, &a[i].r);
    }
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (l(a[i], a[j]) <= sqr(a[i].r + a[j].r)) {
                printf("YES");
                return 0;
            }
        }
    }
    printf("NO");
    return 0;
}
